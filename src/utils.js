export const useTimer = (func, timer) => {
    setTimeout(func, timer);
}