import React, { useState, useEffect } from 'react';
import { Facebook, Instagram, Linkedin, Twitter } from 'react-feather';
import { Link, NavLink, useLocation } from 'react-router-dom';
import './scss/index.scss'
const Footer = () => {
    const location = useLocation();

    const [navStyle, setNavStyle] = useState(false);

    useEffect(() => {
        if (location.pathname === '/projects' || location.pathname === '/about') {
            setNavStyle(true);
        }
        else {
            setNavStyle(false);
        }
    }, [location.pathname])

    const getColor = () => {
        if (location.pathname === '/about') {
            return 'white'
        }
        return '#191919'
    }

    return (
        <footer className="footer" style={navStyle ? { bottom: '46.5%' } : { bottom: 0 }}>
            <div className="footer-container" style={navStyle ? { flexDirection: 'column' } : { flexDirection: 'row' }}>
                <Link to='/' className="footer-link "><Facebook strokeWidth={1} size={15} color={getColor()}/></Link>
                <Link to='/' className="footer-link"><Twitter  strokeWidth={1} size={15} color={getColor()}/></Link>
                <Link to='/' className="footer-link"><Instagram  strokeWidth={1} size={15} color={getColor()}/></Link>
                <Link to='/' className="footer-link"><Linkedin  strokeWidth={1} size={15} color={getColor()}/></Link>
            </div>
        </footer>
    );
};

export default Footer;