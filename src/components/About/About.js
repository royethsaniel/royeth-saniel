import React from 'react';
import './scss/index.scss';
import { motion, useAnimation } from 'framer-motion';
import { useMediaQuery } from 'react-responsive';
import { useEffect, useState } from 'react';
import Animations from './Animations';
import { UserIcon, BriefcaseIcon, BookOpenIcon, HeartIcon, EmojiSadIcon, PlayIcon  } from '@heroicons/react/outline'
import profileSquare from '../../assets/img/photoSquare.jpg';
import Profile from './Profile';
import Work from './Work';
import Skill from './Skill';
import Loader from '../Loader/Loader';
import { Play } from 'react-feather';


const About = () => {

    const [showChevron, setShowChevron] = useState(false);
    const controls = useAnimation();
    const controlsTwo = useAnimation();
    const controlsThree = useAnimation();
    const controlsFour = useAnimation();
    const { scrollDownOne, inInitialTwo, inTransitionAbout, inInitialAbout, inInitialAboutTwo, inInitialAboutThree, inInitialImage, inFinishImage, inFinishAboutTwo, inFinishAbout, inTransitionTwo, inFinish, inInitial, inTransition } = Animations;
    const [showInfo, setShowInfo] = useState(false);
    const [count, setCount] = useState(0);
    const [imgsLoaded, setImgsLoaded] = useState(true);
    const [overlay, setOverlay] = useState(0);
    const [aboutAnimation, setAboutAnimation] = useState(false);

    useEffect(() => {
        controls.start(inFinish);
        setTimeout(() => {
            setImgsLoaded(false)
            setShowChevron(true);
        }, 1000);
    }, []);

    useEffect(() => {
        if (aboutAnimation) {
            controlsTwo.start(inFinishAbout);
            controlsThree.start(inFinishAboutTwo);
        }
    }, [aboutAnimation]);


    const variants = {
        start: {
            width: 0,
            height: '25px',
            border: 'none',
            opacity: 0,
            transition: { duration: 0.5 }
        },
        finish: {
            width: '100%',
            opacity: 1,
            transition: { duration: 1 }
        }
    };
    const variants2 = {
        start: {
            opacity: 0,
            transition: { duration: 0 }
        },
        finish: {
            opacity: 1,
            transition: {
                duration: 1,
                staggerChildren: 0.8
            }
        }
    };

    const item = {
        start: { opacity: 0 },
        finish: { opacity: 1 },
      }

    const drag = { left: -20, right: 20, top: -20, bottom: 20 }

    return (
        <>
        {/* {!imgsLoaded && <Loader route="about"/>} */}
        <motion.div initial={inInitial}
            animate={controls}
            transition={inTransition} className="about">
            <div className="about-hero">
                <div style={{ display: 'flex', alignItems: 'center', marginBottom: 10, marginLeft: 0 }}>
                {showInfo && <motion.span
                        variants={variants}
                        animate={showInfo ? 'finish' : 'start'}
                        style={{ opacity: 0, fontSize: 12, padding: '2px 15px 2px 15px', pointerEvents: 'none',  color: 'white', backgroundColor: '#191919', border: '1px solid #fff',  borderRadius: 50, overflow: 'hidden', width: 0, height: '25px' }}>Tip: Some elements are interactive :)</motion.span>}
                    {!showInfo && <Play onClick={() => { setShowInfo(true); setCount(prev => prev + 1) }} style={{ zIndex: 10, cursor: 'pointer' }} color={'#fff'} size={80} strokeWidth={1.5} />}
                </div>
                {showInfo && 
                <motion.ul
                    variants={variants2}
                    initial={'start'}
                    animate={showInfo ? 'finish' : 'start'}
                    className="py-3 pl-0"
                    style={{listStyleType: 'none'}}
                >
                    <div className='d-flex flex-row justify-content-center'> 
                    {showInfo && <motion.li dragConstraints={drag} drag={true} onClick={() => { setOverlay(1); setCount(3) }} className="about-box" variants={item}><UserIcon color={'#fff'}  width={40} strokeWidth={1.5} /></motion.li>}
                    {showInfo && <motion.li dragConstraints={drag} drag={true} onClick={() => { setOverlay(2); setCount(5) }} className="about-box" variants={item}><BriefcaseIcon color={'#fff'} width={40} strokeWidth={1.5} /></motion.li>}
                    </div>
                    <div className='d-flex flex-row justify-content-center'> 
                    {showInfo && <motion.li dragConstraints={drag} drag={true} onClick={() => { setOverlay(3); setCount(7) }} className="about-box" variants={item}><BookOpenIcon color={'#fff'}  width={40} strokeWidth={1.5} /> </motion.li>}
                    {showInfo && count < 9 && <motion.li dragConstraints={drag} drag={true} onClick={() => { setOverlay(4); setCount(9) }}  className="about-box" variants={item}><HeartIcon color={'#fff'} className={`mx-3 ${count === 9 && 'd-none'}`} width={40} strokeWidth={1.5} /></motion.li>}
                    {showInfo && count >= 9 && <motion.li dragConstraints={drag} className="about-box" variants={item}><EmojiSadIcon color={'#fff'} width={40} strokeWidth={1.5}/></motion.li>}
                    </div>
                </motion.ul>
                }
                {overlay === 1 && <Profile overlay={overlay} setOverlay={setOverlay}/>}
                {overlay === 2 && <Work overlay={overlay} setOverlay={setOverlay}/>}
                {overlay === 3 && <Skill overlay={overlay} setOverlay={setOverlay}/>}
            </div>
        </motion.div>
        </>
    )
}

export default About;