import React from 'react';
import './scss/index.scss';
import { motion, useAnimation } from 'framer-motion';
import { useMediaQuery } from 'react-responsive';
import { useEffect, useState } from 'react';
import { Info, Briefcase, User, BookOpen, Heart, X, XOctagon, XCircle, Command } from 'react-feather';
import Animations from './Animations';
import profileSquare from '../../assets/img/photoSquare.jpg';


const Work = ({overlay, setOverlay}) => {

    const controlsTwo = useAnimation();
    const controlsThree = useAnimation();
    const controlsFour = useAnimation();
    const { scrollDownOne, inInitialTwo, inTransitionAbout, inInitialAbout, inInitialAboutTwo, inInitialAboutThree, inInitialImage, inFinishImage, inFinishAboutTwo, inFinishAbout, inTransitionTwo, inFinish, inInitial, inTransition } = Animations;
    
    const isMobile = useMediaQuery({ maxWidth: 768 });
    const [aboutAnimation, setAboutAnimation] = useState(false);

    useEffect(() => {
        if (aboutAnimation) {
            controlsTwo.start(inFinishAbout);
            controlsThree.start(inFinishAboutTwo);
        }
    }, [aboutAnimation]);

    return (
        <motion.div
        initial={inInitialTwo}
        animate={scrollDownOne}
        transition={inTransitionTwo}
        className="about-overlay border"
        style={aboutAnimation ? { backgroundColor: '#252525' } : { backgroundColor: '#252525' }}
    >
        <motion.div
            initial={inInitialAbout}
            animate={controlsTwo}
            transition={inTransitionAbout}
            className='px-4 px-lg-4 px-xl-5 py-4 pt-md-5 text-white'
        >
            <h1 className="about-title mb-3 mb-lg-5">Work Experiences</h1>
            <motion.div className="row flex-md-row flex-column justify-content-between align-items-start">
                <div className="col-10 col-md-3 border-right about-overlay-height-one">
                    <p>Associate Software Engineer <br/>(Dec 2020 - Present)</p>
                    {!isMobile && <Command style={{position: 'absolute', right: '-0.75rem', top: '0.8rem'}}/>}
                </div>
                <div className="about-body col-12 col-md-8 mt-3 mt-md-0 mb-4 mb-md-0 ">
                    <h5 className='mb-3'>Roles and Responsibilities:</h5>
                    <p className='mb-2'>- Development, enhancement, and maintenance of JoyRide PH Admin</p>
                    <p className='mb-2'>- Experience in leading day-to-day operations of the team through initiation of daily scrum, project deployment, and clarification of project scope and deadline.</p>
                    <p className='mb-2'>- Development, enhancement, and maintenance of an e-commerce web application (JR Mall PH). This includes three web applications:  </p>
                    <p className='mb-3'>-- Storefront (Store website)<br/>-- Seller Center (Seller website)<br/>-- Admin (Admin website)</p>
                    <p className='mb-2'>- Development of new UI features in the mobile application of JoyRide customer app (Android).</p>
                    <p className='mb-2'>- Development of new UI features in the mobile application of JoyRide driver app (Android).</p>
                    <p className='mb-2'>- Development of new features and modules in the admin of new payment method/system (JR Pay).</p>
                    <p className='mb-2'>- Designed and developed new online ordering/booking system to ensure and cater clients, business partners, and customers demand (HappyMove Online Ordering),</p>
                    <p className='mb-2'>- Deployed and developed main product (JoyRide PH) website. </p>
                </div>
           
            </motion.div>
            <motion.div
              initial={{ x:'30%', opacity: 0}}
              transition={{duration: 1}}
              whileInView={{ x: '0', opacity: 1,display: 'flex' }}
              viewport={{ once: true }}
                className="row flex-md-row flex-column justify-content-between align-items-start">
                <div className="col-10 col-md-3 border-right" style={{height: '40vh'}}>
                    <p className='mt-md-3'>Junior Product Engineer <br/>(Sept 2020 - Dec 2020)</p>
                    {!isMobile && <Command style={{position: 'absolute', right: '-0.75rem', top: '1.7rem'}}/>}
                </div>
                <div className="about-body col-12 col-md-8 mt-3 mb-4 mb-md-0">
                    <h5 className='mb-3'>Roles and Responsibilities:</h5>
                    <p className='mb-2'>- Delivers accurate UI/UX wireframes and prototypes.</p>
                    <p className='mb-2'>- Clarification of product/project requirements through wireframes and mockups.</p>
                    <p className='mb-2'>- Provides functional UI components source codes following the mockups.</p>
                    <p className='mb-2'>- Support in ticket creation by adding the concerned mockup and design in the ticket (JIRA)</p>
                    <p className='mb-2'>- Provide up-to-date and brand-based mockup, designs and prototypes</p>
                </div>
            </motion.div>
            <motion.div
              initial={{ x:'30%', opacity: 0}}
              transition={{duration: 1}}
              whileInView={{ x: '0', opacity: 1,display: 'flex' }}
              viewport={{ once: true }}
              className="row flex-md-row flex-column justify-content-between align-items-start">
            <div className="col-10 col-md-3 border-right" style={{height: '10vh'}}>
                {!isMobile && <Command style={{position: 'absolute', right: '-0.75rem', top: '1.7rem'}}/>}
            </div>
            </motion.div>
        </motion.div>
        <X className="about-overlay-close" strokeWidth={3} color={'white'} onClick={() => { setOverlay(0); setAboutAnimation(false) }}>Close</X>
    </motion.div>
    )
}

export default Work;