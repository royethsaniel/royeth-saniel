const Animations = {

    scrollDownOne : {
        y: 0,
        x: 0,
    },
    scrollUpOne : {
        y: 0,
        x: 0,
    },
    scrollDownTwo : {
        y: '-200vh',
        x: 0,
    },
    scrollUpTwo : {
        y: '-100vh',
        x: 0,
    },
    scrollDownThree : {
        y: '-300vh',
        x: 0,
    },
    scrollUpThree : {
        y: '-200vh',
        x: 0,
    },
    finish : {
        opacity: 1,
        y: 0,
        x: 0,
    },
    transition : {
        duration: 1,
        stiffness: 40,
    },
    inInitial :{
        x: '70%',
    },
    inFinish : {
        x: 0,
    },
    inTransition : {
        duration: 1,
    },
    inInitialTwo :{
        x: '30%',
    },
    
    inFinishTwo : {
        x: 0,
    },
    inInitialAbout :{
        y: '0%',
        opacity: 1,
    },
    inFinishAbout: {
        y: '-200%',
        opacity: 0,
        display: 'none'
    },
    inInitialAboutTwo :{
        y: '-100%',
        opacity: 0,
        display: 'none'
    },
    inInitialAboutThree :{
        y: '100%',
        opacity: 0,
        display: 'none'
    },
    inFinishAboutTwo: {
        y: '0%',
        opacity: 1,
        display: 'flex'
    },
    inInitialImage :{
        scale: [1,0.8,0.8,1],
        rotate: [0, 20, -20, 0],
        borderRadius: ["50%", "20%", "20%", "50%"],
    },
    inFinishImage :{
        y: 0,
        opacity: 1,
    },
    inTransitionAbout : {
        duration: 2,
    },
    inTransitionTwo : {
        duration: 1,
    }
}

export default Animations;