import React from 'react';
import './scss/index.scss';
import { motion, useAnimation } from 'framer-motion';
import { useMediaQuery } from 'react-responsive';
import { useEffect, useState } from 'react';
import { Info, Briefcase, User, BookOpen, Heart, X, XOctagon, XCircle, Frown, MapPin } from 'react-feather';
import Animations from './Animations';
import gif from '../../assets/img/loader_about_black.gif';
import profileSquare from '../../assets/img/photoSquare.jpg';
import baguio from '../../assets/img/baguio.jpg';
import pangasinan from '../../assets/img/pangasinan.jpg';
import pampanga from '../../assets/img/pampanga.jpg';
import rizal from '../../assets/img/rizal.jpg';
import quezon from '../../assets/img/quezon.jpg';
import manila from '../../assets/img/manila.jpg';
import Slider from "react-slick";
import Loader from '../Loader/Loader';

const Profile = ({overlay, setOverlay}) => {
    const isMobile = useMediaQuery({ maxWidth: 768 });

    const settings = {
        speed: 1000,
        autoplaySpeed: 2000,
        slidesToShow: 5.5,
        arrows: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                  slidesToShow: 4.5,
                 
                }
            },
            {
                breakpoint: 1200,
                settings: {
                  slidesToShow: 3.5,
                 
                }
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2.5,
               
              }
            },
            {
                breakpoint: 768,
                settings: {
                  slidesToShow: 4.5,
                 
                }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2.5,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2.2,
              }
            }
          ]
    };
    const controlsOne = useAnimation();
    const controlsTwo = useAnimation();
    const controlsThree = useAnimation();
    const controlsFour = useAnimation();
    const { scrollDownOne, inInitialTwo, inTransitionAbout, inInitialAbout, inInitialAboutTwo, inInitialAboutThree, inInitialImage, inFinishImage, inFinishAboutTwo, inFinishAbout, inTransitionTwo, inFinish, inInitial, inTransition } = Animations;
    
    const [aboutAnimation, setAboutAnimation] = useState(false);
    const [imgsLoaded, setImgsLoaded] = useState(false)


    useEffect( async () => {           
        const IMAGES = [gif,profileSquare, baguio, pangasinan, pampanga, rizal, quezon, manila];
        const loadImage = image => {
          return new Promise((resolve, reject) => {
            const loadImg = new Image()
            loadImg.src = image
            // wait 2 seconds to simulate loading time
            loadImg.onload = () =>
              setTimeout(() => {
                resolve(image)
              }, 2000)
    
            loadImg.onerror = err => reject(err)
          })
        }
    
        await Promise.all(IMAGES.map( async image => await loadImage(image)))
          .then(() => setTimeout(()=> setImgsLoaded(true), 1000))
          .catch(err => console.log("Failed to load images", err))
      }, [])

    useEffect(()=>{
        if (imgsLoaded) {
            controlsOne.start(scrollDownOne);
            controlsTwo.start(inFinishAbout);
            controlsThree.start(inFinishAboutTwo);
        }
    },[imgsLoaded]);


    return (
        <>
        {!imgsLoaded && <Loader route="about"/>}
        <motion.div
        initial={inInitialTwo}
        animate={controlsOne}
        transition={inTransitionTwo}
        className="about-overlay border"
        style={aboutAnimation ? { backgroundColor: '#252525' } : { backgroundColor: '#252525' }}
    >
        <X className="about-overlay-close mb-n5" style={{position: 'sticky', top: '1.5rem', float: 'right', zIndex: 100}} strokeWidth={3} color={'white'} onClick={() => { setOverlay(0); setAboutAnimation(false) }}>Close</X>

        {/* <motion.div
            initial={inInitialAbout}
            animate={controlsTwo}
            transition={inTransitionAbout}
            className='px-4 px-md-5 pt-5 pt-md-5 pb-3 text-white'>
            <motion.h1
                className='about-overlay-header'
                initial={{ y: '-500%' }}
                animate={{ y: 0 }}
                transition={{ duration: 1.5 }}
            >
                Hi!
            </motion.h1>
            <p className='about-overlay-text'>First of all, thank you for showing interest in my craft :)</p>
            <p className='about-overlay-text'>I made this portfolio simply for it's sole purpose--to land a job lol</p>
            <p className='about-overlay-text'>Anyway, <a className='about-overlay-text' onClick={() => setAboutAnimation(true)} style={{ borderBottom: '1px solid white', textDecoration: 'none', cursor: 'pointer', color: 'white' }}>see more</a></p>
        </motion.div> */}
        <motion.h1   initial={inInitialAboutTwo}
            animate={controlsThree}
            transition={inTransitionAbout} className="about-title text-white p-4 px-md-5 border-bottom w-100">Personal Information</motion.h1>
        <motion.div
            initial={inInitialAboutTwo}
            animate={controlsThree}
            transition={inTransitionAbout}
            className='text-white pt-4 py-md-5 mb-4 border-bottom pb-4 flex-row flex-lg-column justify-content-start align-items-center align-items-lg-start'>
            <div className='d-flex flex-column align-items-center flex-lg-row px-md-5'>
            <motion.img
                transition={{ duration: 2 }}
                animate={controlsFour}
                className="about-overlay-image"
                src={profileSquare}
            />
            <div className='mx-4 ml-lg-5 text-center text-lg-left'>
                <h1 className='about-overlay-subheader'>Henlo! I'm <span onMouseEnter={() => controlsFour.start(inInitialImage)} onMouseLeave={() => controlsFour.start(inFinishImage)} className='border-bottom'>Roy</span>!</h1>
                <p className='about-overlay-subtext mt-2 mb-0'>An aspiring techy guy who aims to provide valuable digital outputs:
                    graphics, web, and mobile design, source codes, and process automation as a <span className='border-bottom'>developer</span> to a growth-oriented company.</p>
            </div>
            </div>
        </motion.div>
        <motion.div
            initial={inInitialAboutThree} 
            animate={controlsThree}
            transition={inTransitionAbout} 
        className='text-white flex-column mb-4 flex-lg-row justify-content-start align-items-center align-items-lg-center'>
        <div className='w-100'>
            {/* <div className='about-overlay-body-subtext px-md-5 px-3 mt-2 mb-0'>

            </div> */}
            <h4 className='mx-4 mx-md-5 border-bottom py-2' style={{maxWidth: 80}}>Gallery</h4>
            <div style={{position: 'relative', zIndex: 0, transform: 'rotate(-5deg)', marginLeft: '-0.6rem'}}>
            <Slider {...settings}>
                <div>
                    <div>
                    <motion.img
                        transition={{ duration: 2 }}
                        animate={controlsFour}
                        className="about-overlay-image"
                        src={quezon}
                    />
                    <small className='d-block mr-md-4 text-center'>Quezon</small>
                    </div>
                </div>
                <div>
                    <div>
                    <motion.img
                        transition={{ duration: 2 }}
                        animate={controlsFour}
                        className="about-overlay-image"
                        src={manila}
                    />
                    <small className='d-block mr-md-4 text-center'>Manila</small>
                    </div>
                </div>
                <div>
                    <div>
                    <motion.img
                        transition={{ duration: 2 }}
                        animate={controlsFour}
                        className="about-overlay-image"
                        src={rizal}
                    />
                    <small className='d-block mr-md-4 text-center'>Rizal</small>
                    </div>
                </div>
                <div>
                    <div>
                    <motion.img
                        transition={{ duration: 2 }}
                        animate={controlsFour}
                        className="about-overlay-image"
                        src={pampanga}
                    />
                    <small className='d-block mr-md-4 text-center'>Pampanga</small>
                    </div>
                </div>
                <div>
                    <div>
                    <motion.img
                        transition={{ duration: 2 }}
                        animate={controlsFour}
                        className="about-overlay-image"
                        src={pangasinan}
                    />
                    <small className='d-block mr-md-4 text-center'>Pangasinan</small>
                    </div>
                </div>
                <div>
                    <div className=''>
                    <motion.img
                        transition={{ duration: 2 }}
                        animate={controlsFour}
                        className="about-overlay-image"
                        src={baguio}
                    />
                    <small className='d-block mr-md-4 text-center'>Baguio</small>
                    </div>
                </div>
            </Slider>
            </div>
        <small className='text-white pt-5 px-3 text-center d-block'>*Just some of the local places I've been, looking forward for more!</small>
        </div>
        </motion.div>
        
        <motion.div
            className='text-white d-block px-md-5 px-4 border-top py-4'
            initial={inInitialAboutThree} 
            animate={controlsThree}
            transition={inTransitionAbout} 
            whileInView={{ opacity: 1 }}
            viewport={{ once: true }}
        >
            <h4 className='mb-5 py-2 border-bottom' style={{maxWidth: 110}}>About me</h4>
            <div
            initial={{opacity: 0}}
            transition={{duration: 1}} 
            whileInView={{ opacity: 1}}
            className='row'>
                <div className='col-md-8 row'>
                    <motion.div
                        initial={{opacity: 0, y: '50%'}}
                        transition={{duration: 0.5}} 
                        whileInView={{ opacity: 1, y: 0}}
                        className='col-md-6'
                    >
                        <div className='mb-3'>Name: <h5>Royeth Saniel</h5></div>
                        <div className='mb-3'>Age: <h5>23</h5></div>
                        <div className='mb-3'>Address: <h5>Antipolo City, Philippines</h5></div>
                        <div className='mb-3'>Date of Birth: <h5>September 1998</h5></div>
                        <div className='mb-3'>Height: <h5>5'7-ish</h5></div>
                        <div className='mb-3'>Weight: <h5>I'd rather not say</h5></div>
                    </motion.div>
                    <motion.div 
                        className='col-md-6'
                        initial={{opacity: 0, y: '50%'}}
                        transition={{duration: 0.5}} 
                        whileInView={{ opacity: 1, y: 0}}
                    >
                        <div className='mb-3'>Citizenship: <h5>Filipino</h5></div>
                        <div className='mb-3'>Marital Status: <h5>Single</h5></div>
                        <div className='mb-3'>Religion: <h5>Catholic</h5></div>
                        <div className='mb-3'>Blood Type: <h5>I actually don't know lol</h5></div>
                    </motion.div>
                    <motion.div 
                        className='col-md-12 mt-5'
                        initial={{opacity: 0, y: '50%'}}
                        transition={{duration: 0.5}} 
                        whileInView={{ opacity: 1, y: 0}}
                    >
                        <h4 className='mb-5 py-2 border-bottom' style={{maxWidth: 265}}>Educational Attainment</h4>
                        <div className='mb-3'>Tertiary:
                            <h5>University of Rizal System - Morong Campus</h5>
                            <h5>Bachelor of Science in Computer Engineering (BSCpE)</h5>
                            <h6 className='mb-0'>Batch 2019-2020</h6>
                            <h6>Department of Science and Technology Scholar</h6>
                        </div>
                        <div className='mb-3'>Secondary:
                            <h5>San Jose National High School</h5>
                            <h6 className='mb-0'>Batch 2014-2015</h6>
                        </div>
                    </motion.div>
                    <div className="col-md-12 my-5"><h4 className='py-2 border-bottom' style={{maxWidth: 185}}>Hobbies & Sport</h4></div>
                    <motion.div
                        initial={{opacity: 0, y: '50%'}}
                        transition={{duration: 0.5}} 
                        whileInView={{ opacity: 1, y: 0}}
                        className='col-md-6'
                    >
                        <div className='mb-3'>Gaming: 
                            <h5>Valorant</h5>
                            <h5>Mobile Legends</h5>
                            <h5>Genshin Impact</h5>
                        </div>
                    </motion.div>
                    <motion.div 
                        className='col-md-6'
                        initial={{opacity: 0, y: '50%'}}
                        transition={{duration: 0.5}} 
                        whileInView={{ opacity: 1, y: 0}}
                    >
                        <div className='mb-3'>Sport: <h5>Badminton</h5></div>
                    </motion.div>

                       
                </div>
                {!isMobile && 
                <motion.div 
                    className='col-md-4'
                    initial={{opacity: 0, y: '40%'}}
                    transition={{duration: 1}} 
                    whileInView={{ opacity: 1, y: 0}}
                >
                    <img style={{position: 'sticky', top: '20%', height: 'auto', width: '100%'}} src={gif} />
                </motion.div>}
            </div>
        </motion.div>
    </motion.div>
    </>
    )
}

export default Profile;