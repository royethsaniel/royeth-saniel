import React from 'react';
import './scss/index.scss';
import { motion, useAnimation } from 'framer-motion';
import { useMediaQuery } from 'react-responsive';
import { useEffect, useState } from 'react';
import { Adobephotoshop, Adobexd, Angular, Angularjs, Antdesign, Bootstrap, CssThree, Firebase, Framer, Graphql, Html5, Javascript, Jquery, Materialdesign, ReactJs, Redux, Sass, Styledcomponents, Tailwindcss, Typescript } from '@icons-pack/react-simple-icons';
import { Info, Briefcase, User, BookOpen, Heart, X, XOctagon, XCircle, Command } from 'react-feather';
import Animations from './Animations';
import profileSquare from '../../assets/img/photoSquare.jpg';
import Slider from "react-slick";


const Skill = ({overlay, setOverlay}) => {

    const settings = {
        speed: 1000,
        autoplaySpeed: 2000,
        slidesToShow: 5.5,
        arrows: false,
        autoplay: true,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
               
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2.5,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
    };

    const controlsTwo = useAnimation();
    const controlsThree = useAnimation();
    const controlsFour = useAnimation();
    const { scrollDownOne, inInitialTwo, inTransitionAbout, inInitialAbout, inInitialAboutTwo, inInitialAboutThree, inInitialImage, inFinishImage, inFinishAboutTwo, inFinishAbout, inTransitionTwo, inFinish, inInitial, inTransition } = Animations;
    
    const isMobile = useMediaQuery({ maxWidth: 768 });
    const [aboutAnimation, setAboutAnimation] = useState(false);

    useEffect(() => {
        if (aboutAnimation) {
            controlsTwo.start(inFinishAbout);
            controlsThree.start(inFinishAboutTwo);
        }
    }, [aboutAnimation]);

    return (
        <motion.div
        initial={inInitialTwo}
        animate={scrollDownOne}
        transition={inTransitionTwo}
        className="about-overlay border"
        style={aboutAnimation ? { backgroundColor: '#252525' } : { backgroundColor: '#252525' }}
    >
        <motion.div
            initial={inInitialAbout}
            animate={controlsTwo}
            transition={inTransitionAbout}
            className='px-4 px-lg-4 px-xl-5 py-4 pt-md-5 text-white'
        > 
            <h1 className="about-title">Skills</h1>
            <h6 className="mb-3 mb-lg-5">Technologies, frameworks, and softwares used in some of my projects</h6>
            <div className='row'>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Html5/>
                <small className='mt-2'>HTML</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <CssThree/>
                <small className='mt-2'>CSS</small>
              </div> 
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Javascript/>
                <small className='mt-2'>Javascript</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Typescript/>
                <small className='mt-2'>Typescript</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <ReactJs/>
                <small className='mt-2'>React</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Redux/>
                <small className='mt-2'>Redux</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Angular/>
                <small className='mt-2'>Angular</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Graphql/>
                <small className='mt-2'>Graphql</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Firebase/>
                <small className='mt-2'>Firebase</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Jquery/>
                <small className='mt-2'>JQuery</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Tailwindcss/>
                <small className='mt-2'>Tailwind CSS</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Sass/>
                <small className='mt-2'>SASS</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Framer/>
                <small className='mt-2'>Framer Motion</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Styledcomponents/>
                <small className='mt-2'>Styled-components</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Bootstrap/>
                <small className='mt-2'>Bootstrap</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Antdesign/>
                <small className='mt-2'>Ant Design</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Adobexd/>
                <small className='mt-2'>Adobe XD</small>
              </div>
              <div className='about-skills-icons col-md-4 col-lg-3'>
                <Adobephotoshop/>
                <small className='mt-2'>Adobe Photoshop</small>
              </div>
            
       
            </div>
        </motion.div>
        <X className="about-overlay-close" strokeWidth={3} color={'white'} onClick={() => { setOverlay(0); setAboutAnimation(false) }}>Close</X>
    </motion.div>
    )
}

export default Skill;