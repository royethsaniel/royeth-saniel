import React from 'react';
import { Link, NavLink, Redirect } from 'react-router-dom';
import './scss/index.scss';
import {motion, useAnimation} from 'framer-motion';
import { useMediaQuery } from 'react-responsive';
import { useEffect, useState } from 'react';
import { Phone, AtSign, User, BookOpen, Heart, X, XOctagon, XCircle, Mail} from 'react-feather';
import Animations from './Animations';
import bag1 from '../../assets/img/Bag-1.svg';
import bag2 from '../../assets/img/Bag-2.svg';
import bag3 from '../../assets/img/Bag-3.svg';
import bag4 from '../../assets/img/Bag-4.svg';

const Contact = () => {

    const [showChevron, setShowChevron] = useState(false);
    const controls = useAnimation();
    const { scrollDownOne, scrollUpOne, scrollDownTwo, scrollUpTwo, scrollDownThree, scrollUpThree,  transition, inFinish, inInitial, inTransition } = Animations;
    const scroll = window.innerHeight;
    const [showInfo, setShowInfo] = useState(false);
    const [count, setCount] = useState(0);
    const [isActive, setIsActive] = useState(false);
    const [overlay, setOverlay] = useState(0);

    const showBackground = () => {
        setIsActive(true)
    }

    const hideBackground = () => {
        setIsActive(false)
    }

    const scrollTop = () => {
        window.scrollTo(0, scroll);
    }

    const heroStyles = [
        isActive ? {
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.9)',
            marginTop: '16rem'
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)',
            marginTop: '16rem'
        },
        isActive ? {
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.9)',
            marginTop: '16rem'
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)',
            marginTop: '16rem'
        },
        isActive ? {
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.9)',
            marginTop: '16rem'
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)',
            marginTop: '16rem'
        },
        isActive ? {
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.9)',
            marginTop: '16rem'
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)',
            marginTop: '16rem'
        },
    ];

    useEffect(()=>{
        controls.start(inFinish);
        setTimeout(()=>{
            setShowInfo(true);
            setShowChevron(true);
        },1000);
    },[]);

    const item = {
        start: { opacity: 0 },
        finish: { opacity: 1 },
    };

    const variants2 = {
        start: {
            opacity: 0,
            transition: { duration: 0 }
        },
        finish: {
            opacity: 1,
            transition: {
                duration: 1,
                staggerChildren: 0.8
            }
        }
    };
    

    return (
        <motion.div initial={inInitial} 
        animate={controls}
        transition={inTransition}  className="contact position-relative d-flex justify-content-center align-items-center">
            {/* <motion.ul
                variants={variants2}
                initial={'start'}
                animate={showInfo ? 'finish' : 'start'}
                className="py-3 d-flex pl-0"
                style={{listStyleType: 'none'}}
            >
                {showInfo && <motion.li variants={item}><img className='mx-3' src={bag1} width={200} height={200} style={{marginTop: '17rem', cursor: 'pointer'}} onClick={() => { setOverlay(1); setCount(3) }}  /></motion.li>}
                {showInfo && <motion.li variants={item}><img className='mx-3' src={bag2} width={200} height={200} style={{marginTop: '17rem', cursor: 'pointer'}} onClick={() => { setOverlay(2); setCount(5) }}  /></motion.li>}
                {showInfo && <motion.li variants={item}><img className='mx-3' src={bag3} width={200} height={200} style={{marginTop: '17rem', cursor: 'pointer'}} onClick={() => { setOverlay(3); setCount(7) }} /> </motion.li>}
                {showInfo && <motion.li variants={item}><img className='mx-3' src={bag4} width={200} height={200} style={{marginTop: '17rem', cursor: 'pointer'}} onClick={() =>  { setOverlay(4); setCount(9) }} /></motion.li>}
            </motion.ul> */}
            {/* <img onMouseEnter={()=> {setIsActive(true); console.log('test')}} onMouseLeave={()=> {setIsActive(prev => false);}} className='mx-3' src={bag} width={200} height={200} style={heroStyles[0]}/>
            <img className='mx-3' src={bag} width={200} height={200} style={{marginTop: '16rem'}}/>
            <img className='mx-3' src={bag} width={200} height={200} style={{marginTop: '16rem'}}/>
            <img className='mx-3' src={bag} width={200} height={200} style={{marginTop: '16rem'}}/> */}
            
            <div className="contact-hero text-white">
                <h4 className="my-2"><Mail size={30}/>: royethsaniel@gmail.com</h4>
                <h4 className="my-2"><AtSign size={30}/>: rytsnl</h4>
                <h4 className="my-2"><Phone size={30}/>: (+639) 45 375 3912</h4>
                <p style={{fontSize: 8}}>This project is still WIP :P</p>
            </div>
        </motion.div>   
    )
}   

export default Contact;