const Animations = {

    scrollDownOne : {
        y: 0,
        x: 0,
    },
    scrollUpOne : {
        y: 0,
        x: 0,
    },
    scrollDownTwo : {
        y: '-200vh',
        x: 0,
    },
    scrollUpTwo : {
        y: '-100vh',
        x: 0,
    },
    scrollDownThree : {
        y: '-300vh',
        x: 0,
    },
    scrollUpThree : {
        y: '-200vh',
        x: 0,
    },
    finish : {
        opacity: 1,
        y: 0,
        x: 0,
    },
    transition : {
        duration: 1,
        type: "tween",
        stiffness: 40,
    },
    inInitial :{
        x: '60%',
    },
    inFinish : {
        x: 0,
    },
    inTransition : {
        duration: 1,
    }
}

export default Animations;