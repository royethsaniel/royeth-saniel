import React, {useEffect, useState} from 'react';
import { Link, NavLink, useLocation} from 'react-router-dom';
import './scss/index.scss'
const Navbar = () => {

    const location = useLocation();
    
    const [navStyle, setNavStyle] = useState(false);

    useEffect(() => {
        if (location.pathname === '/projects'){
            setNavStyle(true);
        }
        else {
            setNavStyle(false);
        }
    },[location.pathname])
    // #e8e8e8
    return (
        <nav className="navbar" style={navStyle ? {backgroundColor: 'transparent'} : {backgroundColor: 'transparent'}}>
            <div className="navbar-container">
            <NavLink to='/' className={` ${navStyle ? 'navbar-logo-black': 'navbar-logo' }`}>ROYETH SANIEL</NavLink>
            <div className="d-flex">
                <NavLink to='/projects' className={` ${navStyle ? 'navbar-link-black': 'navbar-link' }`}>PROJECTS</NavLink>
                <NavLink to='/about' className={` ${navStyle ? 'navbar-link-black': 'navbar-link' }`}>ABOUT</NavLink>
                <NavLink to='/contact' className={` ${navStyle ? 'navbar-link-black': 'navbar-link' }`}>CONTACT</NavLink>
            </div>
            </div>
        </nav>
    );
};

export default Navbar;