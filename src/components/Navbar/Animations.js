const Animations = {

    initial : {
        opacity: 0,
        y: 0,
        x: 0,
    },
    finish : {
        opacity: 1,
        y: -100,
        x: 0,
    },
    transition : {
        duration: 1,
        type: "spring",
        stiffness: 40,
    }
}

export default Animations;