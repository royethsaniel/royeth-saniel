import React, { useState, useEffect } from 'react';
import { Facebook, Instagram, Linkedin, Twitter } from 'react-feather';
import { Link, NavLink, useLocation } from 'react-router-dom';
import loader from '../../assets/img/loader.gif';
import darkLoader from '../../assets/img/loader_dark.gif';
import './scss/index.scss'
const Loader = ({route}) => {

  const [navStyle, setNavStyle] = useState(false);
  const location = useLocation();

  useEffect(() => {
      if (location.pathname === '/about') {
          setNavStyle(true);
      }
      else {
          setNavStyle(false);
      }
  }, [location.pathname])
    
    return (
      <div className='loader' style={route === 'project' ? {backgroundColor: '#e8e8e8'}: {backgroundColor: '#191919'}}>
          <img src={route === 'project' ? loader: darkLoader} />
      </div>
    );
};

export default Loader;