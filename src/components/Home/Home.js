import './scss/index.scss';
import {  motion, useAnimation } from 'framer-motion';
import {  useEffect, useState } from 'react';
import Animations from './Animations';
import { CornerRightDown, Info } from 'react-feather';
import { NavLink, useHistory } from 'react-router-dom';

const Home = () => {

    const controls = useAnimation();
    const history = useHistory();
    const [isDragged, setIsDragged] = useState(true);
    const [clicks, setClicks] = useState(0);
    const [showInfo, setShowInfo] = useState(false);
    const { initial, finish, transition, outFinish, outInitial, outTransition } = Animations;

    useEffect(() => {
        controls.start(finish);
    }, []);

    const animateNextPage = () => {
        controls.start(outFinish);
        setTimeout(() => {
            history.push('/projects');
        }, 1700)
    }

    const variants = {
        start: {
            width: '0px',
            height: '25px',
            padding: '0 !important',
            border: '1px solid #fff',
            transition: { duration: 0.5 }
        },
        finish: {
            width: '100%',
            height: '25px',
            opacity: 1,
            border: '1px solid #fff',
            transition: { duration: 1 }
        }
    }
    return (
        <motion.div
            initial={outInitial}
            transition={outTransition}
            animate={controls}
            className="home"
        // style={isDragged ? {backgroundImage: `url(${bgTwo})`, transition: 'background-image 1s ease-in-out'}:{backgroundImage: `url(${bg})`, transition: 'background-image 1s ease-in-out'}}
        >
            <div className="home-hero">
                <div className='home-hero-contents'>
                    <motion.h1
                        drag
                        dragDirectionLock
                        initial={initial}
                        animate={controls}
                        transition={transition}
                        style={!isDragged && { color: '#191919', position: 'relative' }}
                    >
                        Less is more <br /> work —
                    </motion.h1>
                    <motion.span drag dragDirectionLock style={!isDragged ? { color: '#191919' } : {}}>Hi! I'm Royeth Saniel a Web, Mobile, UI / UX Designer, and Developer.</motion.span>
                </div>
                <motion.div whileHover={{ scale: 1.4 }}>
                    <NavLink
                        onClick={() => {
                            setClicks(prev => prev + 1);
                            animateNextPage()
                        }}
                        to="/"
                        className="home-hero-link"
                        onMouseEnter={() => setIsDragged(true)}
                        style={!isDragged ? { color: '#191919' } : {}}
                    >
                        Explore Projects <CornerRightDown style={{marginBottom: 3}} strokeWidth={3} size={16}/>
                    </NavLink>
                </motion.div>
                <div style={{ position: 'fixed', display: 'flex', alignItems: 'center', top: '2rem', right: '2rem' }}>
                    <motion.span
                        variants={variants}
                        animate={showInfo ? 'finish' : 'start'}
                        style={{ opacity: 0, fontSize: 12, padding: '2px 33px 2px 10px', pointerEvents: 'none', marginRight: '-1.7rem', color: 'white', backgroundColor: '#191919',  borderRadius: 50, overflow: 'hidden' }}>Tip: Best experience in large devices :)</motion.span>
                    <Info onMouseEnter={() => setShowInfo(true)} onMouseLeave={() => setShowInfo(false)} style={{ zIndex: 10 }} strokeWidth={2} size={30} color={'#fff'} />
                </div>
            </div>

        </motion.div>
    )
}

export default Home;