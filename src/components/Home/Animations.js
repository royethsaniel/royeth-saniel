const Animations = {
    initial : {
        opacity: 0,
        y: 200,
        x: 0,
    },
    finish : {
        opacity: 1,
        y: 0,
        x: 0,
    },
    transition : {
        duration: 1,
        type: "spring",
        stiffness: 40,
    },
    outFinish : {
        y: '-100%',
    },
    outTransition : {
        duration: 2,
    },
}

export default Animations;