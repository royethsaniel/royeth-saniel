const Animations = {

    scrollDownOne : {
        y: '-100vh',
        x: 0,
    },
    scrollUpOne : {
        y: 0,
        x: 0,
    },
    scrollDownTwo : {
        y: '-200vh',
        x: 0,
    },
    scrollUpTwo : {
        y: '-100vh',
        x: 0,
    },
    scrollDownThree : {
        y: '-300vh',
        x: 0,
    },
    scrollUpThree : {
        y: '-200vh',
        x: 0,
    },
    finish : {
        opacity: 1,
        y: 0,
        x: 0,
    },
    transition : {
        duration: 1,
        type: "spring",
        stiffness: 40,
    },
    inInitial :{
        y: '-40%',
    },
    inInitialOne :{
        y: '0',
    },
    inFinishOne :{
        y: 0,
    },
    inFinish : {
        y: 0,
    },
    inTransition : {
        type: "tween",
        duration: 1,
    }
}

export default Animations;