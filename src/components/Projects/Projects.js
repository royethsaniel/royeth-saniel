import React, {useRef} from 'react';
import { Link, NavLink, Redirect } from 'react-router-dom';
import './scss/index.scss';
import {motion, useAnimation} from 'framer-motion';
import { useMediaQuery } from 'react-responsive';
import { useEffect, useState } from 'react';
import Animations from './Animations';
import projectOne from '../../assets/img/jrmall_two.png';
import projectTwo from '../../assets/img/joyride.png';
import projectThree from '../../assets/img/ordering.png';
import projectFour from '../../assets/img/uiux-2.png';
import { ArrowLeft, ChevronDown, ChevronsRight, ChevronUp, ChevronLeft, ChevronRight } from 'react-feather';
import Slider from "react-slick";
import Loader from '../Loader/Loader';

const Projects = () => {

    let slider = useRef()

    const [showChevron, setShowChevron] = useState(false);
    const controlsTwo = useAnimation();
    const controls = useAnimation();
    const { scrollDownOne, scrollUpOne, scrollDownTwo, scrollUpTwo, scrollDownThree, scrollUpThree,  inInitialOne, transition, inFinish, inInitial, inTransition } = Animations;

    const [isActiveOne, setIsActiveOne] = useState(false);
    const [isActiveTwo, setIsActiveTwo] = useState(false);
    const [isActiveThree, setIsActiveThree] = useState(false);
    const [isActiveFour, setIsActiveFour] = useState(false);

    const [isOneToggled, setIsOneToggled] = useState(false);
    const [isTwoToggled, setIsTwoToggled] = useState(false);
    const [isThreeToggled, setIsThreeToggled] = useState(false);
    const [isFourToggled, setIsFourToggled] = useState(false);
    const [isToggled, setIsToggled] = useState(false);
    const [isActive, setIsActive] = useState(false);
    const [imgsLoaded, setImgsLoaded] = useState(false)
    
    useEffect( async () => {           
        const IMAGES = [projectOne, projectTwo, projectThree, projectFour];
        const loadImage = image => {
          return new Promise((resolve, reject) => {
            const loadImg = new Image()
            loadImg.src = image
            // wait 2 seconds to simulate loading time
            loadImg.onload = () =>
              setTimeout(() => {
                resolve(image)
              }, 2000)
    
            loadImg.onerror = err => reject(err)
          })
        }
    
        await Promise.all(IMAGES.map( async image => await loadImage(image)))
          .then(() => setTimeout(()=> setImgsLoaded(true), 1000))
          .catch(err => console.log("Failed to load images", err))
      }, [])

    const showBackground = () => {
        setIsActive(true)
    }

    const hideBackground = () => {
        setIsActive(false)
    }

    const scrollTop = () => {
        window.scrollTo(0, scroll);
    }

    const heroStyles = [
        isActive ? {
            backgroundImage: `url(${projectOne})`, 
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.8)',
            // boxShadow: '10px 10px 30px -5px rgba(99,99,99,0.75)',
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)'
        },
        isActive ? {
            backgroundImage: `url(${projectOne})`, 
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.8)',
            // boxShadow: '10px 10px 30px -5px rgba(99,99,99,0.75)',
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)'
        },
        isActive ? {
            backgroundImage: `url(${projectOne})`, 
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.8)',
            // boxShadow: '10px 10px 30px -5px rgba(99,99,99,0.75)',
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)'
        },
        isActive ? {
            backgroundImage: `url(${projectOne})`, 
            transition: '0.5s ease-in-out', 
            transform: 'scale(0.8)',
            // boxShadow: '10px 10px 30px -5px rgba(99,99,99,0.75)',
        }
        :
        {
            transition: '0.5s ease-in-out', 
            transform: 'scale(1)'
        },
    ];

    const textStyles = [
        isActive ? {opacity: 0}: {opacity: 1, zIndex:10}
    ];

    const activeBorderStyle = {
        width: '100%', borderBottom: '2px solid white', transition: '1s ease-in-out'
    };

    const passiveBorderStyle = {
        width: '0px', borderBottom: '2px solid #E8E8E8', transition: '0.5s ease-in-out'
    }

    const isMobile = useMediaQuery({ maxWidth: 768 });

    useEffect(()=>{
        if (imgsLoaded) {
            controls.start(inFinish);
            setTimeout(()=>{
                setShowChevron(true);
            },1000);
        }
    },[imgsLoaded]);


    const renderClickInfo = () => {
        // let render = false;
        // setTimeout(()=>{
        //     render = !render;
        // },2000);
        return (
            <>
            {
            <div className="w-75 position-absolute" style={{bottom: '2rem', backgroundColor: 'rgba(0,0,0,0.7)', color: "white", textAlign: 'center', pointerEvents: 'none'}}>
                <span style={{fontSize: 18}}>Click anywhere to redirect</span>
            </div>
            }
            </>)
    }
    const scroll = (event, view) => {
        if (event.deltaY < 0) {
            switch (view) {
                case 2:
                    controls.start(scrollUpOne);
                    break;
                case 3:
                    controls.start(scrollUpTwo);
                    break;
                case 4:
                    controls.start(scrollUpThree);
                    break;
            }
        }
        if (event.deltaY > 0) {
            switch (view) {
                case 1:
                    controls.start(scrollDownOne);
                    break;
                case 2:
                    controls.start(scrollDownTwo);
                    break;
                case 3:
                    controls.start(scrollDownThree);
                    break;
            }
        }
    }

    const settings = {
        speed: 500,
        autoplaySpeed: 2000,
        slidesToShow: 1,
        arrows: false,
        autoplay: false,
    };
    const [isLoading, setIsLoading] = useState(true);
    const [mouseOne, setMouseOne] = useState([0,0,false]);
    const [mouseTwo, setMouseTwo] = useState([0,0,false]);
    const [mouseThree, setMouseThree] = useState([0,0,false]);
    const [mouseFour, setMouseFour] = useState([0,0,false]);

    const [mx1, my1, isHover1] = mouseOne;
    const [mx2, my2, isHover2] = mouseTwo;
    const [mx3, my3, isHover3] = mouseThree;
    const [mx4, my4, isHover4] = mouseFour;


    const handleMouseMove = (e, setMouse) => {
        const { offsetTop, offsetLeft } = e.currentTarget;
        setMouse([e.pageX - offsetLeft, e.pageY - offsetTop, true]);
    }


    const renderDetailsView = () => {
        return (
            <>
        {isOneToggled &&
            <motion.div initial={inInitialOne}
                animate={controlsTwo}
                transition={inTransition}
                className={`container ${!isMobile && 'vh-100 justify-content-center'} d-flex align-items-start flex-column projects-details`}>
                <h1 className='mb-5'>E-commerce</h1>
                <div className=' text-left'>
                    <h2>JR Mall Storefront</h2>
                    <p>Order delivery whenever and wherever! Find the best local restaurants, caterers, grocers, stores, and more! <br />Download the JoyRide app or visit the JR Mall website to get everything you need!</p>
                    <div style={{ cursor: 'pointer' }} onClick={() => window.open("https://jrmall.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                        <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                        <ChevronsRight/>
                    </div>
                </div>
                <div className='my-4 text-left'>
                    <h2>JR Mall Seller Center</h2>
                    <p>Manage your orders wherever you are with your JR Mall Seller Center! <br />In just a few clicks, be able to fulfill orders and book instant deliveries via JoyRide and Happy Move.</p>
                    <div style={{ cursor: 'pointer' }} onClick={() => window.open("https://seller.jrmall.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                        <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                        <ChevronsRight/>
                    </div>
                </div>
                <div className='my-4 text-left'>
                    <h2>JR Mall Admin</h2>
                    <div style={{ cursor: 'pointer' }} onClick={() => window.open("https://admin.jrmall.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                        <h6 className='text-black mr-2  mb-0'>Visit site</h6>
                        <ChevronsRight/>
                    </div>
                </div>

                <div style={{ cursor: 'pointer' }} onClick={() => {setIsOneToggled(prev=> !prev); setIsToggled(false); slider.slickGoTo(0, true)}} className="mt-5 text-left d-flex align-items-center">
                    <ArrowLeft/>
                    <h5 className='ml-2'>Go back</h5>
                </div>
            </motion.div>
            }
            {isTwoToggled &&
                <motion.div initial={!isMobile && {y: '100vh'}}
                    transition={inTransition}
                className={`container ${!isMobile && 'vh-100 justify-content-center'} d-flex align-items-start flex-column projects-details`}>
                    <h1 className='mb-5'>Web Applications</h1>

                    <div className='my-4 text-left'>
                        <h2>Personal Portfolio</h2>
                        {/* <p>Order delivery whenever and wherever! Find the best local restaurants, caterers, grocers, stores, and more! <br />Download the JoyRide app or visit the JR Mall website to get everything you need!</p> */}
                        <div className="mt-3 text-left d-flex align-items-center">
                            {/* <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                            <ChevronsRight/> */}
                        </div>
                    </div>
                    <div className='my-4 text-left'>
                        <h2>JoyRide Website</h2>
                        {/* <p>Order delivery whenever and wherever! Find the best local restaurants, caterers, grocers, stores, and more! <br />Download the JoyRide app or visit the JR Mall website to get everything you need!</p> */}
                        <div style={{ cursor: 'pointer' }} onClick={() => window.open("https://joyride.com.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                            <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                            <ChevronsRight/>
                        </div>
                    </div>
                    <div className='my-4 text-left'>
                        <h2>Happy Move Online Ordering</h2>
                        {/* <p>Manage your orders wherever you are with your JR Mall Seller Center! <br />In just a few clicks, be able to fulfill orders and book instant deliveries via JoyRide and Happy Move.</p> */}
                        <div style={{ cursor: 'pointer' }} onClick={() => window.open("https://online.joyride.com.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                            <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                            <ChevronsRight/>
                        </div>
                    </div>
                    <div style={{ cursor: 'pointer' }} onClick={() => {setIsTwoToggled(prev=> !prev); setIsToggled(false); slider.slickGoTo(1, true)}}  className="mt-5 text-left d-flex align-items-center">
                        <ArrowLeft/>
                        <h5 className='ml-2'>Go back</h5>
                    </div>
                </motion.div>
            }
            {isThreeToggled &&
            <motion.div initial={!isMobile && {y: '200vh'}}
                transition={inTransition}
                className={`container ${!isMobile && 'vh-100 justify-content-center'} d-flex align-items-start flex-column projects-details`}>
                <h1 className='mb-5'>Internal Systems</h1>
                <div className='my-4 text-left'>
                    <h2>JoyRide Admin</h2>
                    {/* <p>Order delivery whenever and wherever! Find the best local restaurants, caterers, grocers, stores, and more! <br />Download the JoyRide app or visit the JR Mall website to get everything you need!</p> */}
                    <div style={{ cursor: 'pointer' }}  onClick={() => window.open("https://admin.joyride.com.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                        <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                        <ChevronsRight/>
                    </div>
                </div>
                <div className='my-4 text-left'>
                    <h2>JR Pay Admin</h2>
                    {/* <p>Manage your orders wherever you are with your JR Mall Seller Center! <br />In just a few clicks, be able to fulfill orders and book instant deliveries via JoyRide and Happy Move.</p> */}
                    <div style={{ cursor: 'pointer' }} onClick={() => window.open("https://admin.jrpay.ph", "_blank")} className="mt-3 text-left d-flex align-items-center">
                        <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                        <ChevronsRight/>
                    </div>
                </div>
                <div style={{ cursor: 'pointer' }} onClick={() => {setIsThreeToggled(prev=> !prev); setIsToggled(false); slider.slickGoTo(2, true)}} className="mt-5 text-left d-flex align-items-center">
                    <ArrowLeft/>
                    <h5 className='ml-2'>Go back</h5>
                </div>
            </motion.div>
        }
     
        {isFourToggled &&
            <motion.div 
                initial={!isMobile && {y: '300vh'}}
                transition={inTransition}
                className={`container ${!isMobile && 'vh-100 justify-content-center'} d-flex align-items-start flex-column projects-details`}>
                <h1 className='mb-5'>UI / UX</h1>
                <div className='my-4 text-left'>
                    <h2>Behance</h2>
                    {/* <p>Order delivery whenever and wherever! Find the best local restaurants, caterers, grocers, stores, and more! <br />Download the JoyRide app or visit the JR Mall website to get everything you need!</p> */}
                    <div style={{ cursor: 'pointer' }}  onClick={() => window.open("https://www.behance.net/royethsani7c86", "_blank")} className="mt-3 text-left d-flex align-items-center">
                        <h6 className='text-black mr-2 mb-0'>Visit site</h6>
                        <ChevronsRight/>
                    </div>
                </div>
                <div style={{ cursor: 'pointer' }}  onClick={() => {setIsFourToggled(prev=> !prev); setIsToggled(false); slider.slickGoTo(3, true)}} className="mt-5 text-left d-flex align-items-center">
                    <ArrowLeft/>
                    <h5 className='ml-2'>Go back</h5>
                </div>
            </motion.div>
        }
        </>
        );
    }

    const mobileView = () => {
        return (
        <motion.div initial={inInitial} 
        animate={controls}
        transition={inTransition} className="projects">
          <Slider ref={ref=> slider = ref} className={`${!isToggled && 'projects-slider'}`} {...settings}>
          {!isToggled && 
                <div 
                    id="project-one"
                    className="text-primary projects-hero text-center position-relative" 
                    style={heroStyles[0]}
                >
                    <ChevronLeft onClick={()=> slider.slickGoTo(3, true)} style={{marginLeft: '1rem', marginBottom: '3rem'}}/>
                    <h2 style={textStyles[0]} onClick={() => { setIsToggled(true); setIsOneToggled(prev=> !prev); controlsTwo.start(inFinish) }}>E-commerce</h2>
                    <ChevronRight onClick={()=> slider.slickGoTo(1)}  style={{marginRight: '1rem', marginBottom: '3rem'}}/>
                </div>}
                {!isToggled && 
                <div 
                    id="project-two" 
                    style={heroStyles[1]}
                    className="text-primary projects-hero text-center position-relative"
                >
                    <ChevronLeft onClick={()=> slider.slickGoTo(0)}  style={{marginLeft: '1rem', marginBottom: '3rem'}}/>
                    <h2 style={isActive ? {opacity: 0}: {opacity: 1}} onClick={() => { setIsToggled(true); setIsTwoToggled(prev=> !prev); controlsTwo.start(inFinish) }}>Web Applications</h2>
                    <ChevronRight onClick={()=> slider.slickGoTo(2)}  style={{marginRight: '1rem', marginBottom: '3rem'}}/>
                </div>}
                 {!isToggled && 
                <div 
                    id="project-three"
                    style={heroStyles[2]}
                    className="text-primary projects-hero text-center position-relative"
                >
                    <ChevronLeft onClick={()=> slider.slickGoTo(1)}  style={{marginLeft: '1rem', marginBottom: '3rem'}}/>
                    <h2 style={isActive ? {opacity: 0}: {opacity: 1}} onClick={() => { setIsToggled(true); setIsThreeToggled(prev=> !prev); controlsTwo.start(inFinish) }}>Internal Systems</h2>
                    <ChevronRight onClick={()=> slider.slickGoTo(3)}  style={{marginRight: '1rem', marginBottom: '3rem'}}/>
                </div>}
                 {!isToggled && 
                <div 
                    id="project-four" 
                    style={heroStyles[3]}
                    className="text-primary projects-hero text-center position-relative"
                >
                    <ChevronLeft onClick={()=> slider.slickGoTo(2)} style={{marginLeft: '1rem', marginBottom: '3rem'}}/>
                    <h2 style={isActive ? {opacity: 0}: {opacity: 1}} onClick={() => { setIsToggled(true); setIsFourToggled(prev=> !prev); controlsTwo.start(inFinish) }}>UI / UX</h2>
                    <ChevronRight onClick={()=> slider.slickGoTo(4)}  style={{marginRight: '1rem', marginBottom: '3rem'}}/>
                </div>}
            </Slider>
            {renderDetailsView()}
        </motion.div>
        )
    }
   

    return (
        <>
        {!imgsLoaded && <Loader route="project"/>}
        {isMobile ? mobileView() :
        <motion.div initial={inInitial} 
        animate={controls}
        transition={inTransition}  className="projects">
            {/* <div className="projects-hero"> */}
                {!isToggled &&
                <div 
                    onWheel={(event)=> scroll(event, 1)}
                    id="project-one"
                    className="text-primary projects-hero position-relative" 
                    style={heroStyles[0]}
                >
                    <h2 
                      onMouseMove={e => handleMouseMove(e, setMouseOne)}
                      onMouseEnter={() => setMouseOne([mx1, my1, true])}
                      onMouseLeave={() => setMouseOne([mx1, my1, false])}
                    style={textStyles[0]}  onClick={() => { setIsToggled(true); setIsOneToggled(prev=> !prev); controlsTwo.start(inFinish) }}>E-commerce</h2>
                   <motion.div 
                    style={{position: 'absolute'}}
                    initial={{
                        opacity: 0,
                        visibility: 0,
                        x: 300,
                        y: 0
                    }}
                    animate={{
                        x: mx1 + 200,
                        y: my1,
                        opacity: 1,
                    }}
                    exit={{
                        opacity: 0,
                    }}
                    ><img style={mouseOne[2] ? {opacity: 1, borderRadius: '1rem'} : {opacity: 0}} src={projectOne} width={'60%'}/></motion.div>
                    {isActive && renderClickInfo()}
                    {!isMobile && <ChevronDown onClick={()=> controls.start(scrollDownOne)} className={`fas fa-chevron-down projects-hero-down ${isActive || !showChevron ? 'active' : 'inactive'}`}></ChevronDown>}
                </div>
                }
                {!isToggled &&
                <div id="project-two" 
                onWheel={(event)=> scroll(event, 2)}
                style={heroStyles[1]}
                className="text-primary projects-hero"
                >
                    {isActive && renderClickInfo()}
                    <h2 style={textStyles[0]} 
                    onMouseMove={e => handleMouseMove(e, setMouseTwo)}
                    onMouseEnter={() => setMouseTwo([mx2, my2, true])}
                    onMouseLeave={() => setMouseTwo([mx2, my2, false])}
                    onClick={() => { setIsToggled(true); setIsTwoToggled(prev=> !prev); controlsTwo.start(inFinish) }}
                    >
                        Web Applications
                    </h2>
                    <motion.div 
                        style={{position: 'absolute'}}
                        initial={{
                            opacity: 0,
                            visibility: 0,
                            x: 300,
                            y: 0 
                        }}
                        animate={{
                            x: mx2 + 200,
                            y: my2,
                            opacity: 1,
                        }}
                        exit={{
                            opacity: 0,
                        }}
                    >
                        <img style={mouseTwo[2] ? {opacity: 1, borderRadius: '1rem'} : {opacity: 0}} src={projectTwo} width={'60%'}/>
                    </motion.div>
                    {!isMobile && <ChevronDown onClick={()=> controls.start(scrollDownTwo)} className={`fas fa-chevron-down projects-hero-down ${isActive || !showChevron ? 'active' : 'inactive'}`}></ChevronDown>}
                    {!isMobile && <ChevronUp onClick={()=> controls.start(scrollUpOne)} className={`fas fa-chevron-up projects-hero-up ${isActive || !showChevron ? 'active' : 'inactive'}`}></ChevronUp>}
                </div>
                }
                {!isToggled &&
                <div id="project-three"
                onWheel={(event)=> scroll(event, 3)}
                style={heroStyles[2]}
                className="text-primary projects-hero">
                  <h2 style={textStyles[0]} 
                    onMouseMove={e => handleMouseMove(e, setMouseThree)}
                    onMouseEnter={() => setMouseThree([mx3, my3, true])}
                    onMouseLeave={() => setMouseThree([mx3, my3, false])}
                    onClick={() => { setIsToggled(true); setIsThreeToggled(prev=> !prev); controlsTwo.start(inFinish) }}
                    >
                        Internal Systems
                    </h2>
                    <motion.div 
                        style={{position: 'absolute'}}
                        initial={{
                            opacity: 0,
                            x: 300,
                            y: 0 
                        }}
                        animate={{
                            x: mx3 + 200,
                            y: my3,
                            opacity: 1,
                        }}
                        exit={{
                            opacity: 0,
                        }}
                    >
                        <img style={mouseThree[2] ? {opacity: 1, borderRadius: '1rem'} : {opacity: 0}} src={projectThree} width={'60%'}/>
                    </motion.div>
                    {!isMobile && <ChevronDown onClick={()=> controls.start(scrollDownThree)} className={`fas fa-chevron-down projects-hero-down ${isActive || !showChevron ? 'active' : 'inactive'}`}></ChevronDown>}
                    {!isMobile && <ChevronUp onClick={()=> controls.start(scrollUpTwo)} className={`fas fa-chevron-up projects-hero-up ${isActive || !showChevron ? 'active' : 'inactive'}`}></ChevronUp>}
                </div>}
                {!isToggled &&
                <div id="project-four" 
                onWheel={(event)=> scroll(event, 4)}
                style={heroStyles[3]}
                className="text-primary projects-hero">
                <h2 style={textStyles[0]} 
                    onMouseMove={e => handleMouseMove(e, setMouseFour)}
                    onMouseEnter={() => setMouseFour([mx4, my4, true])}
                    onMouseLeave={() => setMouseFour([mx4, my4, false])}
                    onClick={() => { setIsToggled(true); setIsFourToggled(prev=> !prev); controlsTwo.start(inFinish) }}
                    >
                        UI / UX
                    </h2>
                    <motion.div 
                        style={{position: 'absolute'}}
                        initial={{
                            opacity: 0,
                            x: 250,
                            y: 0 
                        }}
                        animate={{
                            x: mx4 + 250,
                            y: my4,
                            opacity: 1,
                        }}
                        exit={{
                            opacity: 0,
                        }}
                    >
                        <img style={mouseFour[2] ? {opacity: 1, borderRadius: '1rem'} : {opacity: 0}} src={projectFour} width={'60%'}/>
                    </motion.div>
                {!isMobile && <ChevronUp onClick={()=> controls.start(scrollUpThree)} className={`fas fa-chevron-up projects-hero-up ${isActive || !showChevron ? 'active' : 'inactive'}`}></ChevronUp>}
                </div>}
                {renderDetailsView()}
        </motion.div>   
    }
    </>
    )
}

export default Projects;
