// import './App.css';
import React, {useEffect, useState} from 'react';
import { BrowserRouter, Route, Switch, useLocation } from 'react-router-dom';
import Home from './components/Home/Home';
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Projects from "./components/Projects/Projects";
import About from "./components/About/About";
import Contact from "./components/Contact/Contact";

const App = () => {
  const location = useLocation();
  const [isHome, setIsHome] = useState(false);

  useEffect(()=>{
    const spinner = document.getElementById('spinner');
    // spinner.parentNode.removeChild(spinner);
    if(location.pathname === '/') {
      setIsHome(true);
    }
    else {
      setIsHome(false);
    }
  },[location.pathname])
  return (
    <>
      {!isHome && <Navbar/>}
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/projects" component={Projects}/>
        <Route path="/about" component={About}/>
        <Route path="/contact" component={Contact}/>
      </Switch>
      <Footer/>
    </>
  );
}

export default App;
